package org.abh.photos.profile;

import com.google.gson.JsonObject;

import org.abh.photos.home.PhotoFollowingModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by binhjy on 08/01/2017.
 */

public interface FollowInterface {
    @POST("/api/follows/{targetUserId}")
    Call<JsonObject> followUser(@Header("Authorization") String authorization, @Path("targetUserId") String targetUserId);

    @DELETE("/api/follows/{targetUserId}")
    Call<JsonObject> unFollowUser(@Header("Authorization") String authorization, @Path("targetUserId") String targetUserId);

    @GET("/api/follows/photos")
    Call<List<PhotoFollowingModel>> getAllPhotoFollowing(@Header("Authorization") String authorization);
}
