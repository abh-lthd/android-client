package org.abh.photos.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.abh.photos.R;
import org.abh.photos.account.CredentialManager;
import org.abh.photos.rest.ServiceFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by binhjy on 04/01/2017.
 */

public class ProfileFragment extends Fragment {
    private TextView mTvUsername;
    private TextView mTvEmail;
    private ImageView mImvAvatar;
    private TextView mTvDescription;
    private Button mBtFollow;
    private String mUserIdCurrent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        mTvUsername = (TextView)view.findViewById(R.id.profile_tvUsername);
        mTvEmail = (TextView)view.findViewById(R.id.profile_tvEmail);
        mImvAvatar = (ImageView)view.findViewById(R.id.profile_imvAvatar);
        mTvDescription = (TextView)view.findViewById(R.id.profile_tvDescription);
        mBtFollow = (Button) view.findViewById(R.id.profile_btFollow);

        String username = (String)getArguments().getSerializable("username");
        String email = (String)getArguments().getSerializable("email");
        String avatarUrl = (String)getArguments().getSerializable("avatarUrl");
        String description = (String)getArguments().getSerializable("description");
        mUserIdCurrent = (String)getArguments().getSerializable("userId");

        mTvUsername.setText(username);
        mTvEmail.setText(email);
        mTvDescription.setText(description);
        Picasso.with(getContext())
                .load(avatarUrl)
                .into(mImvAvatar);

        mBtFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBtFollow.getText().equals(getResources().getString(R.string.follow))){
                    mBtFollow.setText(getResources().getString(R.string.following));
                    FollowUser();
                }
                else {
                    mBtFollow.setText(getResources().getString(R.string.follow));
                    UnFollowUser();
                }
            }
        });

        return view;
    }

    private void UnFollowUser() {
        FollowInterface followInterface = ServiceFactory.createService(FollowInterface.class);
        String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();
        Call<JsonObject> followCall = followInterface.unFollowUser(authorization, mUserIdCurrent);
        followCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()){
                    Toast.makeText(getContext(), "UnFollow successully", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getContext(), "UnFollow failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void FollowUser() {
        FollowInterface followInterface = ServiceFactory.createService(FollowInterface.class);
        String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();
        Call<JsonObject> followCall = followInterface.followUser(authorization, mUserIdCurrent);
        followCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()){
                    Toast.makeText(getContext(), "Follow successully", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getContext(), "Follow failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


}
