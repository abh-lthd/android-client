package org.abh.photos.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class FragmentAnimation {
    public static void navigateToFragment(FragmentActivity activity, Fragment fragment,
                                          int replaceFragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = activity
                .getSupportFragmentManager()
                .beginTransaction();

        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,
                android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        ft.replace(replaceFragment, fragment, tag);

        if (addToBackStack) {
            ft.addToBackStack(null);
        }

        ft.commit();
    }
}
