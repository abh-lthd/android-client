package org.abh.photos.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import org.abh.photos.R;
import org.abh.photos.account.LoginFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentAnimation.navigateToFragment(this, new LoginFragment(),
                R.id.fragment_container_activity, "LoginFragment", false);

//        Fragment fragment = new LoginFragment();
//        getSupportFragmentManager()
//                .beginTransaction()
//                .add(R.id.main_fragment_container, fragment, "LoginFragment")
//                .commit();

        // Initialize Fb SDK & Log Fb app event
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());
    }
}
