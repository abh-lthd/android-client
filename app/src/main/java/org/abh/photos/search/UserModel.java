package org.abh.photos.search;

import com.google.gson.annotations.SerializedName;

import org.abh.photos.rest.ServiceFactory;

/**
 * Created by binhjy on 08/01/2017.
 */

public class UserModel {
    @SerializedName("_id")
    private String mId;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("avatar")
    private String mAvatar;
    @SerializedName("description")
    private String mDescription;

    public UserModel(String id, String username, String email, String avatar, String description){
        mId = id;
        mUsername = username;
        mEmail = email;
        mAvatar = avatar;
        mDescription = description;
    }

    public void setId(String id){mId = id;}

    public String getId(){return mId;}

    public void setUsername(String username){mUsername = username;}

    public String getUsername(){return mUsername;}

    public void setEmail(String email){mEmail = email;}

    public String getEmail(){return mEmail;}

    public void setAvatar(String avatar){mAvatar = avatar;}

    public void setDescription(String description){mDescription = description;}

    public String getDescription(){return mDescription;}

    public String getAvatarUrl(){
        String avataUrl = ServiceFactory.BASE_URL + "/" + mAvatar;
        return avataUrl;
    }

}
