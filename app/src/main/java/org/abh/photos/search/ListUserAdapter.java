package org.abh.photos.search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.abh.photos.R;
import org.abh.photos.profile.ProfileFragment;
import org.abh.photos.ui.FragmentAnimation;

import java.util.ArrayList;

/**
 * Created by binhjy on 08/01/2017.
 */

public class ListUserAdapter extends ArrayAdapter<UserModel> {
    private Context mContext;
    private ArrayList<UserModel> mLstUser = new ArrayList<>();

    public ListUserAdapter(Context context, ArrayList<UserModel> lstUser){
        super(context, R.layout.list_search_item, lstUser);
        mContext = context;
        mLstUser = lstUser;
    }

    static class ViewHolder{
        ImageView imvAvatar;
        TextView tvUsername;
        TextView tvEmail;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.list_search_item, null);
            viewHolder = new ViewHolder();
            viewHolder.imvAvatar = (ImageView)convertView.findViewById(R.id.list_search_item_imvAvatar);
            viewHolder.tvUsername = (TextView)convertView.findViewById(R.id.list_search_item_tvUsername);
            viewHolder.tvEmail = (TextView)convertView.findViewById(R.id.list_search_item_tvEmail);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.tvUsername.setText(mLstUser.get(position).getUsername());
        viewHolder.tvEmail.setText(mLstUser.get(position).getEmail());
        String avatarUrl = mLstUser.get(position).getAvatarUrl();
        Picasso.with(mContext)
                .load(avatarUrl)
                .fit()

                .into(viewHolder.imvAvatar);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileFragment fmProfile = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("username", mLstUser.get(position).getUsername());
                bundle.putSerializable("email", mLstUser.get(position).getEmail());
                bundle.putSerializable("avatarUrl", mLstUser.get(position).getAvatarUrl());
                bundle.putSerializable("description", mLstUser.get(position).getDescription());
                bundle.putSerializable("userId", mLstUser.get(position).getId());
                fmProfile.setArguments(bundle);

                FragmentAnimation.navigateToFragment((FragmentActivity)mContext, fmProfile, R.id.fragment_container,
                        "ProfileFragment", true);

            }
        });

        return convertView;
    }
}
