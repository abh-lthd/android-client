package org.abh.photos.search;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by binhjy on 08/01/2017.
 */

public interface SearchInterface {
    @GET("/api/users")
    Call<List<UserModel>> searchUser(@Query("username") String username);
}
