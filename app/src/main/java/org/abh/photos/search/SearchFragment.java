package org.abh.photos.search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.abh.photos.R;
import org.abh.photos.rest.ServiceFactory;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by binhjy on 08/01/2017.
 */

public class SearchFragment extends Fragment implements Callback<List<UserModel>> {
    private EditText mEdtSearch;
    private ListView mLvUser;
    private ImageView mImvSearch;
    private ArrayList<UserModel> mLstUser;
    private ListUserAdapter mLstUserAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        mEdtSearch = (EditText)view.findViewById(R.id.search_edtSearch);
        mLvUser = (ListView)view.findViewById(R.id.search_lvUser);
        mImvSearch = (ImageView)view.findViewById(R.id.search_imvSearch);


        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mEdtSearch.getText().toString().length() == 0){
                    //mEdtComment.setError(getResources().getString(R.string.comment_error));
                }
                else {
                    mEdtSearch.setError(null);
                }
            }
        });

        mImvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mEdtSearch.getText().toString().length() == 0){
                    mEdtSearch.setError(getResources().getString(R.string.search_error));
                }
                else {
                    mEdtSearch.setError(null);
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    CallSearch();
                }

            }
        });

        mLstUser = new ArrayList<>();
        mLstUserAdapter = null;
        return view;
    }

    private void CallSearch() {
        SearchInterface searchInterface = ServiceFactory.createService(SearchInterface.class);
        Call<List<UserModel>> userCall = searchInterface.searchUser(mEdtSearch.getText().toString().trim());
        userCall.enqueue(this);
    }


    @Override
    public void onResponse(Call<List<UserModel>> call, Response<List<UserModel>> response) {
        if(response.isSuccessful()){
            if(response.body().size() > 0) {
                mLstUser.clear();
                for (UserModel user : response.body()) {
                    mLstUser.add(user);
                }

                ShowUser();

            }
        }
        else {
            Toast.makeText(getContext(), "Search failed", Toast.LENGTH_LONG).show();
        }
    }

    private void ShowUser() {
        if(mLstUser.size() > 0){
            mLstUserAdapter = new ListUserAdapter(getActivity(), mLstUser);
            mLvUser.setAdapter(mLstUserAdapter);
        }
    }

    @Override
    public void onFailure(Call<List<UserModel>> call, Throwable t) {
        Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }
}
