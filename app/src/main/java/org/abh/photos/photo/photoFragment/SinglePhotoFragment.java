package org.abh.photos.photo.photoFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.abh.photos.R;

import java.util.Date;

/**
 * Created by binhjy on 03/01/2017.
 */

public class SinglePhotoFragment extends Fragment {
    private ImageView mImvPhoto;
    private TextView mTvDate;
    private TextView mTvStatus;
    private ImageView mImvLike;
    private TextView mTvNumberOfLikes;
    private LinearLayout mLnlNumberOfLikes;
    private ImageView mImvComment;
    private ListView mLvCommentPhoto;

    private static Date date;
    private static String status;
    private static String photoUrl;
    private static String photoId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_photo, container, false);
        mImvPhoto = (ImageView)view.findViewById(R.id.single_photo_imvPhoto);
        mTvDate = (TextView)view.findViewById(R.id.single_photo_tvDate);
        mTvStatus = (TextView)view.findViewById(R.id.single_photo_tvStatus);
        mImvLike = (ImageView)view.findViewById(R.id.single_photo_imvLike);
        mTvNumberOfLikes = (TextView)view.findViewById(R.id.single_photo_tvNumberOfLikes);
        mLnlNumberOfLikes = (LinearLayout)view.findViewById(R.id.single_photo_lnlNumberOfLikes);
        mImvComment = (ImageView)view.findViewById(R.id.single_photo_imvComment);
        mLvCommentPhoto = (ListView)view.findViewById(R.id.single_photo_lvCommentPhoto);

        if(getArguments().getSerializable("photoDate") != null)
            date = (Date) getArguments().getSerializable("photoDate");
        if(getArguments().getSerializable("photoStatus") != null)
            status = (String)getArguments().getSerializable("photoStatus");
        if(getArguments().getSerializable("photoUrl") != null)
            photoUrl = (String)getArguments().getSerializable("photoUrl");
        if(getArguments().getSerializable("photoId") != null)
            photoId = (String)getArguments().getSerializable("photoId");

        SinglePhotoBusiness.InitDataControl(getActivity(), photoId,
                mLnlNumberOfLikes, mTvNumberOfLikes, mImvLike, mLvCommentPhoto, false);
        SinglePhotoBusiness.SetSourcePhoto(getActivity(), mImvPhoto, photoUrl);
        mTvDate.setText(date.toString());
        mTvStatus.setText(status);

        mImvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SinglePhotoBusiness.OnClickImvLike(mImvLike, getActivity(), photoId, mLnlNumberOfLikes, mTvNumberOfLikes);
            }
        });

        mImvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SinglePhotoBusiness.OnClickImvComment(getActivity(), photoId);
            }
        });

        view.setPadding(8, 8, 8, 8);
        return view;
    }




}
