package org.abh.photos.photo.photoFragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.abh.photos.R;
import org.abh.photos.account.CredentialManager;
import org.abh.photos.photo.photoInterface.PhotoService;
import org.abh.photos.photo.photoCallback.UploadServiceCallback;
import org.abh.photos.photo.photoModel.Photo;
import org.abh.photos.rest.ServiceFactory;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by binhjy on 04/01/2017.
 */

public class UploadFragment extends Fragment {
    private ImageView mImvPhoto;
    private EditText mEdtStatus;
    private byte[] bPhoto = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload, container, false);
        mImvPhoto = (ImageView)view.findViewById(R.id.upload_imvPhoto);
        mEdtStatus = (EditText)view.findViewById(R.id.upload_edtStatus);

        bPhoto = (byte[])getArguments().getSerializable("bPhoto");
        Bitmap bmPhoto = BitmapFactory.decodeByteArray(bPhoto, 0 , bPhoto.length);
        mImvPhoto.setImageBitmap(bmPhoto);
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        float spWidth = displayMetrics.widthPixels;
        float spHeight = displayMetrics.heightPixels;
        int imageWidth = (int) (spWidth);
        int imageHeight = (int)(spHeight - 600);
        LinearLayout.LayoutParams layoutPhoto = new LinearLayout.LayoutParams(imageWidth, imageHeight);
        layoutPhoto.gravity = Gravity.CENTER_HORIZONTAL;
        mImvPhoto.setLayoutParams(layoutPhoto);
        view.setPadding(8, 8, 8, 8);


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.upload_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.upload_menu_done:{
                if (bPhoto != null) {
                    PhotoService photoService = ServiceFactory.createService(PhotoService.class);
                    String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();
                    RequestBody photo = RequestBody.create(MediaType.parse("image/*"), bPhoto);
                    RequestBody status = RequestBody.create(MediaType.parse("text/plain"), mEdtStatus.getText().toString().trim());
                    Call<Photo> uploadCall = photoService.upload(authorization, photo, status);
                    uploadCall.enqueue(new UploadServiceCallback(getActivity()));
                }
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
