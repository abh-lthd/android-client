package org.abh.photos.photo.photoFragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.TextView;

import org.abh.photos.R;
import org.abh.photos.home.PhotoFragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoChoosingDialogFragment extends DialogFragment {
    private static final int REQUEST_PHOTO_CAPTURE = 2;
    private static final int REQUEST_PHOTO_SELECT = 3;
    private static String mCurrentPhotoPath;
    private TextView tvTakePhoto;

    public PhotoChoosingDialogFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_select_photo_method);

        View view = View.inflate(getActivity(), R.layout.dialog_choose_photo, null);
        tvTakePhoto = (TextView)view.findViewById(R.id.tv_take_a_photo);
        tvTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePhotoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(getActivity(), "org.abh.photos", photoFile);
                        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePhotoIntent, REQUEST_PHOTO_CAPTURE);
                    }
                }
            }
        });


        TextView tvChooseFromFile = (TextView)view.findViewById(R.id.tv_choose_from_file_system);
        tvChooseFromFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectPhotoIntent = new Intent();
                selectPhotoIntent.setType("image/*");
                selectPhotoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(selectPhotoIntent, "Select Photo"), REQUEST_PHOTO_SELECT);
            }
        });

        builder.setView(view);

        return builder.create();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFilename = "JPEG_" + timeStamp;
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFilename, ".png", storageDir);

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bitmap bitmapPhoto = null;

            if (requestCode == REQUEST_PHOTO_CAPTURE) {
                bitmapPhoto = BitmapFactory.decodeFile(mCurrentPhotoPath);
            } else if (requestCode == REQUEST_PHOTO_SELECT) {
                Uri selectedPhotoUri = data.getData();
                try {
                    bitmapPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedPhotoUri);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            PhotoFragment homeFragment = (PhotoFragment)getTargetFragment();
            homeFragment.onPhotoReadyForUpload(bitmapPhoto);
        }
    }

}
