package org.abh.photos.photo.photoCallback;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import org.abh.photos.R;
import org.abh.photos.account.CredentialManager;
import org.abh.photos.photo.photoAdapter.GridPhotoAdapter;
import org.abh.photos.photo.photoAdapter.ListPhotoAdapter;
import org.abh.photos.photo.photoInterface.PhotoService;
import org.abh.photos.photo.photoModel.PhotoModel;
import org.abh.photos.rest.ServiceFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class GetAllPhotosServiceCallback implements Callback<List<PhotoModel>> {
    private static Context mContext;
    private static ArrayList<PhotoModel> mLstPhoto;
    private static GridView mGvPhoto;
    private static ListView mLvPhoto;
    private static GridPhotoAdapter mGridPhotoAdapter;
    private static ListPhotoAdapter mListPhotoAdapter;
    private static boolean mIsViewOnGrid = true;
    private static MenuItem mMenuItemViewPhoto;


    public GetAllPhotosServiceCallback(Context context, GridView gvPhoto, ListView lvPhoto,
                                       MenuItem menuItemViewPhoto) {
        mContext = context;
        mGvPhoto = gvPhoto;
        mLvPhoto = lvPhoto;
       // mIsViewOnGrid = isViewOnGrid;
        GridPhotoAdapter mGridPhotoAdapter = null;
        ListPhotoAdapter mListPhotoAdapter = null;
        mLstPhoto = new ArrayList<>();
        mMenuItemViewPhoto = menuItemViewPhoto;
        mIsViewOnGrid = !mIsViewOnGrid;
    }

    @Override
    public void onResponse(Call<List<PhotoModel>> call, Response<List<PhotoModel>> response) {
        if (response.isSuccessful()) {
            if(response.body().size() > 0) {
                // mLstPhoto.clear();
                for (PhotoModel photoModel : response.body()) {
                    mLstPhoto.add(photoModel);
                }

                ViewPhoto();

            }
        } else {
            // Toast.makeText(mContext, "ShowImage failed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<List<PhotoModel>> call, Throwable t) {
        Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    public static void ViewPhoto(){
        if(mLstPhoto.size() > 0) {
            if(!mIsViewOnGrid) {
                mIsViewOnGrid = true;
                mLvPhoto.setVisibility(View.GONE);
                mGvPhoto.setVisibility(View.VISIBLE);
                mGridPhotoAdapter = new GridPhotoAdapter(mContext, mLstPhoto);
                mGvPhoto.setAdapter(mGridPhotoAdapter);
                if(mMenuItemViewPhoto != null)
                    mMenuItemViewPhoto.setIcon(R.drawable.ic_listview);
            }
            else {
                mIsViewOnGrid = false;
                mGvPhoto.setVisibility(View.GONE);
                mLvPhoto.setVisibility(View.VISIBLE);
                mListPhotoAdapter = new ListPhotoAdapter(mContext, mLstPhoto, mLvPhoto);
                mLvPhoto.setAdapter(mListPhotoAdapter);
                //mLvPhoto.invalidate();

                if(mMenuItemViewPhoto != null)
                    mMenuItemViewPhoto.setIcon(R.drawable.ic_gridview);
            }
        }
    }

    public static void LoadPhoTo(Context context, GridView gvPhoto, ListView lvPhoto,
                                 MenuItem menuItemViewPhoto){
        PhotoService photoService = ServiceFactory.createService(PhotoService.class);
        String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();
        Call<List<PhotoModel>> getAllPhotoCall = photoService.getAllPhotos(authorization);
        if(context != null && gvPhoto != null && lvPhoto != null)
            getAllPhotoCall.enqueue(new GetAllPhotosServiceCallback(context, gvPhoto, lvPhoto, menuItemViewPhoto));
        else
            getAllPhotoCall.enqueue(new GetAllPhotosServiceCallback(mContext, mGvPhoto, mLvPhoto, menuItemViewPhoto));
    }

    public static void SortListPhoTo(SORTMODE sortmode){
        if (sortmode == SORTMODE.AS) {
            Collections.sort(mLstPhoto, new Comparator<PhotoModel>() {
                public int compare(PhotoModel o1, PhotoModel o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });
        }
        else {
            Collections.sort(mLstPhoto, new Comparator<PhotoModel>() {
                public int compare(PhotoModel o1, PhotoModel o2) {
                    return o2.getDate().compareTo(o1.getDate());
                }
            });
        }
        ViewPhoto();
       // ViewPhoto(VIEWPHOTOMODE.OnList);
       // LoadPhoTo(null,null,null);
    }

    public enum VIEWPHOTOMODE {
        OnGrid,
        OnList
    }

    public enum SORTMODE{
        AS,
        DES
    }

}
