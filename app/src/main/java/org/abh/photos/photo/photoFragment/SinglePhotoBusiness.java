package org.abh.photos.photo.photoFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.abh.photos.R;
import org.abh.photos.account.CredentialManager;
import org.abh.photos.photo.photoCallback.GetCommentPhotoCallback;
import org.abh.photos.photo.photoCallback.GetLikePhotoCallback;
import org.abh.photos.photo.photoInterface.PhotoActionInterface;
import org.abh.photos.rest.ServiceFactory;
import org.abh.photos.ui.FragmentAnimation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by binhjy on 05/01/2017.
 */

public class SinglePhotoBusiness {

    public static void InitDataControl(Context context, String photoId, LinearLayout lnlNumberOfLikes, TextView tvNumberOfLikes,
                                       ImageView imvLike, ListView lvCommentPhoto, boolean isSingleComment){
        GetLikePhotoCallback.getLikePhoto(context, photoId, lnlNumberOfLikes, tvNumberOfLikes, imvLike);
        GetCommentPhotoCallback.LoadCommentPhoto(context, lvCommentPhoto, photoId, isSingleComment);

    }

    public static void SetSourcePhoto(Context context, ImageView imvPhoto, String photoUrl) {
        Picasso.with(context)
                .load(photoUrl)
                .fit()
                .centerCrop().into(imvPhoto);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float spWidth = displayMetrics.widthPixels;
        float spHeight = displayMetrics.heightPixels;
        int imageWidth = (int) (spWidth - 25);
        int imageHeight = (int)(spHeight - 700);
        LinearLayout.LayoutParams layoutPhoto = new LinearLayout.LayoutParams(imageWidth, imageHeight);
        layoutPhoto.gravity = Gravity.CENTER_HORIZONTAL;
        imvPhoto.setLayoutParams(layoutPhoto);
    }

    public static void OnClickImvLike(final ImageView imvLike, final Context context, final String photoId,
                                      final LinearLayout lnlNumberOfLikes, final TextView tvNumberOfLikes){
        String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();
        if(imvLike.getTag().equals("ic_like")) {
            imvLike.setImageResource(R.drawable.ic_liked);
            imvLike.setTag("ic_liked");

            PhotoActionInterface photoActionInterface = ServiceFactory.createService(PhotoActionInterface.class);
            Call<JsonObject> likePhoto = photoActionInterface.like(authorization, photoId);
            likePhoto.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if(response.isSuccessful()){
                        JsonObject jsonObject = response.body();
                        GetLikePhotoCallback.getLikePhoto(context, photoId, lnlNumberOfLikes, tvNumberOfLikes, imvLike);
                        int numberOfLikes = GetLikePhotoCallback.getNumberOfLikes() + 1;
                        UpdateLikePhoto(lnlNumberOfLikes, tvNumberOfLikes, imvLike, numberOfLikes, true);

                    }
                    else {
                        //Toast.makeText(context, "Like failed", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
        else {
            imvLike.setImageResource(R.drawable.ic_like);
            imvLike.setTag("ic_like");
            PhotoActionInterface photoActionInterface = ServiceFactory.createService(PhotoActionInterface.class);
            Call<JsonObject> unlikePhoto = photoActionInterface.unlike(authorization, photoId);
            unlikePhoto.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    int numberOfLikes = 0;
                    if(response.isSuccessful()){
                        GetLikePhotoCallback.getLikePhoto(context, photoId, lnlNumberOfLikes, tvNumberOfLikes, imvLike);

                        if(GetLikePhotoCallback.getNumberOfLikes() > 0) {
                            numberOfLikes = GetLikePhotoCallback.getNumberOfLikes()  - 1;

                        }
                    }
                    else {
                        //Toast.makeText(context, "UnLike failed", Toast.LENGTH_LONG).show();
                    }
                    UpdateLikePhoto(lnlNumberOfLikes, tvNumberOfLikes, imvLike, numberOfLikes, false);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public static void UpdateLikePhoto(LinearLayout lnlNumberOfLikes, TextView tvNumberOfLikes, ImageView imvLike,
                                       int numberOfLike, boolean isCurrentUserLiked){
        if(numberOfLike > 0)
            lnlNumberOfLikes.setVisibility(View.VISIBLE);
        else
            lnlNumberOfLikes.setVisibility(View.GONE);
        tvNumberOfLikes.setText(numberOfLike + " likes");
        if(isCurrentUserLiked) {
            imvLike.setImageResource(R.drawable.ic_liked);
            imvLike.setTag("ic_liked");
        }
        else {
            imvLike.setImageResource(R.drawable.ic_like);
            imvLike.setTag("ic_like");
        }
    }

    public static void OnClickImvComment(FragmentActivity activity, String photoId){
        Fragment fragment = new CommentFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("photoId", photoId);
        fragment.setArguments(bundle);
        FragmentAnimation.navigateToFragment(activity, fragment, R.id.fragment_container,
                "CommentFragment", true);
//        activity.get

//        FragmentTransaction ft = context.getCh
//                .beginTransaction();
//
//        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,
//                android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//
//        ft.replace(replaceFragment, fragment, tag);
//
//        if (addToBackStack) {
//            ft.addToBackStack(null);
//        }
//
//        ft.commit();

//        CommentFragment nextFrag= new CommentFragment();
//        a
    }

}
