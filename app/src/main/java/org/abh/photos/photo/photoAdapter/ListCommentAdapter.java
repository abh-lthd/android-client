package org.abh.photos.photo.photoAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.abh.photos.R;
import org.abh.photos.photo.photoModel.CommentModel;

import java.util.ArrayList;

/**
 * Created by binhjy on 05/01/2017.
 */

public class ListCommentAdapter extends ArrayAdapter<CommentModel> {
    private Context mContext;
    private ArrayList<CommentModel> mLstCommentPhoto;
    private boolean mIsSinglePhoto;
    private LayoutInflater mInflater;

    public ListCommentAdapter(Context context, ArrayList<CommentModel> lstCommentPhoto,boolean isSinglePhoto){
        super(context, R.layout.list_comment_item, lstCommentPhoto);
        mContext = context;
        mLstCommentPhoto = lstCommentPhoto;
        mIsSinglePhoto = isSinglePhoto;
//        mInflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder{
        ImageView imvAvatar;
        TextView tvUsername;
        TextView tvComment;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.list_comment_item, null);
            viewHolder = new ViewHolder();
            viewHolder.imvAvatar = (ImageView)convertView.findViewById(R.id.list_comment_item_imvAvatar);
            viewHolder.tvUsername = (TextView)convertView.findViewById(R.id.list_comment_item_tvUsername);
            viewHolder.tvComment = (TextView)convertView.findViewById(R.id.list_comment_item_tvComment);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        if(!mIsSinglePhoto){
            viewHolder.imvAvatar.setVisibility(View.GONE);
        }
        else
            viewHolder.imvAvatar.setVisibility(View.VISIBLE);
        viewHolder.tvUsername.setText(mLstCommentPhoto.get(position).getUsername());
        viewHolder.tvComment.setText(mLstCommentPhoto.get(position).getComment());

        return convertView;
    }
}
