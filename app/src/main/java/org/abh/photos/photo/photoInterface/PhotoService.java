package org.abh.photos.photo.photoInterface;

import org.abh.photos.photo.photoModel.Photo;
import org.abh.photos.photo.photoModel.PhotoModel;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by chuzzz on 1/1/2017.
 */

public interface PhotoService {
    @Multipart
    @POST("/api/photos/upload")
    Call<Photo> upload(@Header("Authorization") String authorization, @Part("photo\"; filename=\"photo.png\" ") RequestBody file, @Part("status") RequestBody status);

    @GET("/api/photos")
    Call<List<PhotoModel>> getAllPhotos(@Header("Authorization") String authorization);

    @GET("/api/photos/{photoId}")
    Call<String> getPhotoUrl(@Path("photoId") String photoId);

}