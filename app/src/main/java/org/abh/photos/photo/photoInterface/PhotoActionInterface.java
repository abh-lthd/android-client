package org.abh.photos.photo.photoInterface;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by binhjy on 04/01/2017.
 */

public interface PhotoActionInterface {
    @POST("/api/likes/{photoId}")
    Call<JsonObject> like(@Header("Authorization") String authorization, @Path("photoId") String photoId);

    @GET("/api/likes/{photoId}")
    Call<JsonArray> getLikePhoto(@Path("photoId") String photoId);

    @DELETE("/api/likes/{photoId}")
    Call<JsonObject> unlike(@Header("Authorization") String authorization, @Path("photoId") String photoId);


    @POST("/api/comments/")
    Call<JsonObject> comment(@Header("Authorization") String authorization, @Body JsonObject commentRequest);

    @GET("/api/comments/{photoId}")
    Call<JsonArray> getCommentPhoto(@Path("photoId") String photoId);
}
