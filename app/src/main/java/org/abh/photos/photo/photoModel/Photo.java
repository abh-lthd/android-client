package org.abh.photos.photo.photoModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class Photo {
    @SerializedName("photoId")
    private String mId;

    public Photo(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
