package org.abh.photos.photo.photoModel;

/**
 * Created by binhjy on 05/01/2017.
 */

public class CommentModel {
    private String mIdUser;
    private String mUsername;
    private String mComment;

    public void setIdUser(String idUser){mIdUser = idUser;}

    public String getIdUser(){return mIdUser;}

    public void setUsername(String username){mUsername = username;}

    public String getUsername(){return mUsername;}

    public void setComment(String comment){mComment = comment;}

    public String getComment(){return mComment;}
}
