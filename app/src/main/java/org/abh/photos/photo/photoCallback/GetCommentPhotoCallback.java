package org.abh.photos.photo.photoCallback;


import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.abh.photos.account.CredentialManager;
import org.abh.photos.photo.photoAdapter.ListCommentAdapter;
import org.abh.photos.photo.photoInterface.PhotoActionInterface;
import org.abh.photos.photo.photoModel.CommentModel;
import org.abh.photos.rest.ServiceFactory;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by binhjy on 05/01/2017.
 */

public class GetCommentPhotoCallback implements Callback<JsonArray> {
    private static Context mContext;
    private static ArrayList<CommentModel> mLstCommentPhoto;
    private static ListView mLvCommentPhoto;
    private static ListCommentAdapter mLstCommentAdapter;
    private static boolean mIsSingleComment;

    public GetCommentPhotoCallback(Context context, ListView lvCommentPhoto, boolean isSingleComment){
        mContext = context;
        mLvCommentPhoto = lvCommentPhoto;
        mLstCommentPhoto = new ArrayList<>();
        mLstCommentAdapter = null;
        mIsSingleComment = isSingleComment;
    }

    @Override
    public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
        if(response.isSuccessful()){
            JsonArray jsonArray = response.body();
            for(JsonElement jsonElement : jsonArray){
                String comment = jsonElement.getAsJsonObject().get("comment").getAsString();
                String username = jsonElement.getAsJsonObject().get("user").getAsJsonObject().get("username").getAsString();
                String userId = jsonElement.getAsJsonObject().get("user").getAsJsonObject().get("username").getAsString();
                CommentModel commentModel = new CommentModel();
                commentModel.setIdUser(userId);
                commentModel.setUsername(username);
                commentModel.setComment(comment);
                mLstCommentPhoto.add(commentModel);

            }
            ShowComment();
        }
        else {
            //Toast.makeText(mContext, "GetComment failed", Toast.LENGTH_LONG).show();
//            mLstCommentPhoto = new ArrayList<>();
        }
        //ShowComment();
    }

    private void ShowComment() {
        //if(mLstCommentPhoto.size() > 0) {
            mLstCommentAdapter = new ListCommentAdapter(mContext, mLstCommentPhoto, mIsSingleComment);
            mLvCommentPhoto.setAdapter(mLstCommentAdapter);
       // }
    }

    @Override
    public void onFailure(Call<JsonArray> call, Throwable t) {
        Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    public static void LoadCommentPhoto(Context context, ListView lvCommentPhoto, String photoId, boolean isSingleComment){
        PhotoActionInterface photoActionInterface = ServiceFactory.createService(PhotoActionInterface.class);
        Call<JsonArray> getCommentPhoto = photoActionInterface.getCommentPhoto(photoId);
        getCommentPhoto.enqueue(new GetCommentPhotoCallback(context, lvCommentPhoto, isSingleComment));
    }

    public static void OnClickImvSend(final String photoId, final Context context, final ListView lvCommentPhoto,
                                      final EditText edtComment){
        PhotoActionInterface photoActionInterface = ServiceFactory.createService(PhotoActionInterface.class);
        String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("photo_id", photoId);
        jsonObject.addProperty("comment", edtComment.getText().toString().trim());

        Call<JsonObject> commentPhoto = photoActionInterface.comment(authorization, jsonObject);
        commentPhoto.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()){
                    LoadCommentPhoto(context, lvCommentPhoto, photoId, true);
                }

                InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtComment.getWindowToken(), 0);
                edtComment.setText("");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }


}
