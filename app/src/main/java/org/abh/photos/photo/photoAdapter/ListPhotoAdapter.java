package org.abh.photos.photo.photoAdapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.abh.photos.R;
import org.abh.photos.photo.photoFragment.SinglePhotoBusiness;
import org.abh.photos.photo.photoFragment.SinglePhotoFragment;
import org.abh.photos.photo.photoModel.PhotoModel;
import org.abh.photos.ui.FragmentAnimation;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by binhjy on 03/01/2017.
 */

public class ListPhotoAdapter extends ArrayAdapter<PhotoModel> {
    private Context mContext;
    private ArrayList<PhotoModel> mLstPhoto = new ArrayList<>();
    private ListView mLvPhoto;

    public ListPhotoAdapter(Context context, ArrayList<PhotoModel> lstphoto, ListView lvPhoto){
        super(context, R.layout.list_comment_item, lstphoto);
        mContext = context;
        mLstPhoto = lstphoto;
        mLvPhoto = lvPhoto;
    }


    static class ViewHolder{
        TextView tvDate;
        ImageView imvPhoto;
        TextView tvStatus;
        ListView lvCommentPhoto;
        LinearLayout lnlNumberOfLikes;
        ImageView imvLike;
        TextView tvNumberOfLikes;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        LayoutInflater inflater = null;
        if(convertView == null){
            inflater = (LayoutInflater)mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//            View itemView = inflater.inflate(R.layout.fragment_single_photo, null, true);
            convertView = inflater.inflate(R.layout.fragment_single_photo, null);
            viewHolder = new ViewHolder();
            viewHolder.tvDate = (TextView)convertView.findViewById(R.id.single_photo_tvDate);
            viewHolder.imvPhoto = (ImageView)convertView.findViewById(R.id.single_photo_imvPhoto);
            viewHolder.tvStatus = (TextView)convertView.findViewById(R.id.single_photo_tvStatus);
            viewHolder.lnlNumberOfLikes = (LinearLayout)convertView.findViewById(R.id.single_photo_lnlNumberOfLikes);
            viewHolder.imvLike = (ImageView)convertView.findViewById(R.id.single_photo_imvLike);
            viewHolder.tvNumberOfLikes = (TextView)convertView.findViewById(R.id.single_photo_tvNumberOfLikes);
            viewHolder.lvCommentPhoto = (ListView)convertView.findViewById(R.id.single_photo_lvCommentPhoto);


            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        final String photoId = mLstPhoto.get(position).getId();
        final Date date = mLstPhoto.get(position).getDate();
        final String status = mLstPhoto.get(position).getStatus();
        final String photoUrl = mLstPhoto.get(position).getPhotoUrl();

        SinglePhotoBusiness.InitDataControl(mContext, photoId,
                viewHolder.lnlNumberOfLikes, viewHolder.tvNumberOfLikes, viewHolder.imvLike, viewHolder.lvCommentPhoto, false);
        SinglePhotoBusiness.SetSourcePhoto(mContext, viewHolder.imvPhoto, photoUrl);
        viewHolder.tvDate.setText(date.toString());
        viewHolder.tvStatus.setText(status);

        viewHolder.imvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SinglePhotoBusiness.OnClickImvLike(viewHolder.imvLike, mContext, photoId, viewHolder.lnlNumberOfLikes,
                        viewHolder.tvNumberOfLikes);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment_detail_photo = new SinglePhotoFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("photoDate", date);
                bundle.putSerializable("photoStatus", status);
                bundle.putSerializable("photoUrl", photoUrl);
                bundle.putSerializable("photoId", photoId);
                fragment_detail_photo.setArguments(bundle);
                FragmentAnimation.navigateToFragment((FragmentActivity)mContext, fragment_detail_photo, R.id.fragment_container,
                        "DetailPhotoFragment", true);
            }
        });

       // Utility.setListViewHeightBasedOnChildren(mLvPhoto);

        ViewGroup.LayoutParams layoutParams = viewHolder.lvCommentPhoto.getLayoutParams();
        layoutParams.height = 100;
        viewHolder.lvCommentPhoto.setLayoutParams(layoutParams);
        mLvPhoto.invalidate();
       // mLvPhoto.requestLayout();

        //View viewOutner = inflater.inflate(R.id, null);
        return convertView;

    }

}
