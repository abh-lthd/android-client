package org.abh.photos.photo.photoAdapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import org.abh.photos.R;
import org.abh.photos.photo.photoFragment.SinglePhotoFragment;
import org.abh.photos.photo.photoModel.PhotoModel;
import org.abh.photos.ui.FragmentAnimation;

import java.util.ArrayList;

/**
 * Created by binhjy on 02/01/2017.
 */

public class GridPhotoAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PhotoModel> mLstPhoto;

    public GridPhotoAdapter(Context context, ArrayList<PhotoModel> lstPhoto){
        mContext = context;
        mLstPhoto = lstPhoto;
    }

    @Override
    public int getCount() {
        return mLstPhoto.size();
    }

    @Override
    public Object getItem(int i) {
        return mLstPhoto.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View gridview = new View(mContext);
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view != null) {
            gridview = inflater.inflate(R.layout.grid_photo_item, null);
            ImageView imvPhoto = (ImageView)gridview.findViewById(R.id.grid_photo_item_imvImage);

            Picasso.with(mContext)
                    .load(mLstPhoto.get(i).getPhotoUrl())
                    .fit()
                    .centerCrop().into(imvPhoto);
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            float sppHeight = displayMetrics.heightPixels;
            float spWidth = displayMetrics.widthPixels;
            int imageWidth = (int) (spWidth / 2 - 20);
            imvPhoto.setLayoutParams(new LinearLayout.LayoutParams(imageWidth, imageWidth));
            gridview.setPadding(8, 8, 8, 8);

            gridview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment_detail_photo = new SinglePhotoFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("photoDate", mLstPhoto.get(i).getDate());
                    bundle.putSerializable("photoStatus", mLstPhoto.get(i).getStatus());
                    bundle.putSerializable("photoUrl", mLstPhoto.get(i).getPhotoUrl());
                    bundle.putSerializable("photoId", mLstPhoto.get(i).getId());
                    fragment_detail_photo.setArguments(bundle);

                    FragmentAnimation.navigateToFragment((FragmentActivity)mContext, fragment_detail_photo, R.id.fragment_container,
                            "DetailPhotoFragment", true);
                }
            });
        }


        return gridview;
    }


}
