package org.abh.photos.photo.photoFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import org.abh.photos.R;
import org.abh.photos.photo.photoCallback.GetCommentPhotoCallback;

/**
 * Created by binhjy on 05/01/2017.
 */

public class CommentFragment extends Fragment {
    private ListView mLvCommentPhoto;
    private ImageView mImvSend;
    private EditText mEdtComment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener( new View.OnKeyListener()
//        {
//
//
//            @Override
//            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
//                if( keyCode == KeyEvent.KEYCODE_BACK ) {
//                    getFragmentManager().popBackStack();
//                   // getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                    return true;
//                }
//                return false;
//            }
//        } );
    }

    @Override
    public void onPause() {
        super.onPause();
//        FragmentAnimation.navigateToFragment(getActivity(), new SinglePhotoFragment(), R.id.main_fragment_container,
//                            "SinglePhotoFragment", false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment, container, false);
        mLvCommentPhoto = (ListView)view.findViewById(R.id.comment_lvCommentPhoto);
        mImvSend = (ImageView)view.findViewById(R.id.comment_imvSend);
        mEdtComment = (EditText)view.findViewById(R.id.comment_edtComment);

        final String photoId = (String)getArguments().getSerializable("photoId");

        GetCommentPhotoCallback.LoadCommentPhoto(getActivity(), mLvCommentPhoto, photoId, true);

        mEdtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mEdtComment.getText().toString().length() == 0){
                    //mEdtComment.setError(getResources().getString(R.string.comment_error));
                }
                else {
                    mEdtComment.setError(null);
                }
            }
        });

        mImvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mEdtComment.getText().toString().length() == 0){
                    mEdtComment.setError(getResources().getString(R.string.comment_error));
                }
                else {
                    mEdtComment.setError(null);
                    GetCommentPhotoCallback.OnClickImvSend(photoId,
                            getActivity(), mLvCommentPhoto, mEdtComment);
                }

            }
        });

//        Fragment videoFragment = new VideoPlayerFragment();
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        transaction.add(R.id.video_fragment, videoFragment).commit();

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
//                if( keyCode == KeyEvent.KEYCODE_BACK ) {
//                    //getFragmentManager().popBackStack();
//                    // getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                    return true;
//                }
                return false;
            }
        } );




        return view;
    }




}
