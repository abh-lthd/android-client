package org.abh.photos.photo.photoCallback;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import org.abh.photos.account.CredentialManager;
import org.abh.photos.photo.photoFragment.SinglePhotoBusiness;
import org.abh.photos.photo.photoInterface.PhotoActionInterface;
import org.abh.photos.rest.ServiceFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by binhjy on 04/01/2017.
 */

public class GetLikePhotoCallback implements Callback<JsonArray> {
    private Context mContext;
    private static int mNumberOfLikes = 0;
    private static boolean mIsCurrentUserLiked = false;
    private LinearLayout mLnlNumberOfLikes;
    private TextView mTvNumberOfLikes;
    private ImageView mImvLike;

    public GetLikePhotoCallback(Context context, LinearLayout lnlNumberOfLikes, TextView tvNumberOfLikes,
                                ImageView imvLike){
        mContext = context;
        mLnlNumberOfLikes = lnlNumberOfLikes;
        mTvNumberOfLikes = tvNumberOfLikes;
        mImvLike = imvLike;
        mIsCurrentUserLiked = false;
        mNumberOfLikes = 0;
    }


    @Override
    public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

        if (response.isSuccessful()) {
            JsonArray jsonArray = response.body();
            for(JsonElement jsonElement : jsonArray){
                String idUser  = jsonElement.getAsJsonObject().get("user").getAsJsonObject().get("_id").getAsString();
                if(idUser.equals(CredentialManager.getInstance().getId())){
                    mIsCurrentUserLiked = true;
                }
            }
            mNumberOfLikes = jsonArray.size();

        } else {
           // Toast.makeText(mContext, "GetLike failed", Toast.LENGTH_LONG).show();
             mNumberOfLikes = 0;
        }
        SinglePhotoBusiness.UpdateLikePhoto(mLnlNumberOfLikes, mTvNumberOfLikes, mImvLike,
                mNumberOfLikes, mIsCurrentUserLiked);

    }

    @Override
    public void onFailure(Call<JsonArray> call, Throwable t) {
        Toast.makeText(mContext, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    public static int getNumberOfLikes(){
        return mNumberOfLikes;
    }

    public static boolean IsCurrentUserLiked(){return mIsCurrentUserLiked;}

    public static void getLikePhoto(Context context, String photoId, LinearLayout lnlNumberOfLikes,
                                    TextView tvNumberOfLikes, ImageView imvLike){
        PhotoActionInterface photoActionInterface = ServiceFactory.createService(PhotoActionInterface.class);
        Call<JsonArray> getLikePhoto = photoActionInterface.getLikePhoto(photoId);
        getLikePhoto.enqueue(new GetLikePhotoCallback(context, lnlNumberOfLikes, tvNumberOfLikes, imvLike));
    }


}
