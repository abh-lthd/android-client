package org.abh.photos.photo.photoCallback;

import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import org.abh.photos.R;
import org.abh.photos.home.PhotoFragment;
import org.abh.photos.photo.photoModel.Photo;
import org.abh.photos.ui.FragmentAnimation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class UploadServiceCallback implements Callback<Photo> {
    private FragmentActivity mFmActivity;

    public UploadServiceCallback(FragmentActivity fmActivity) {
        mFmActivity = fmActivity;
    }

    @Override
    public void onResponse(Call<Photo> call, Response<Photo> response) {
        if (response.isSuccessful()) {
            Toast.makeText(mFmActivity, "Upload successfully", Toast.LENGTH_LONG).show();
            FragmentAnimation.navigateToFragment(mFmActivity, new PhotoFragment(), R.id.fragment_container,
                    "PhotoFragment", true);
            //GetAllPhotosServiceCallback.LoadPhoTo(null, null, null);
        } else {
            Toast.makeText(mFmActivity, "Upload failed", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<Photo> call, Throwable t) {
        Toast.makeText(mFmActivity, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }
}
