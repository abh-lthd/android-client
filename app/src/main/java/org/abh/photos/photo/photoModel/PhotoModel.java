package org.abh.photos.photo.photoModel;

import com.google.gson.annotations.SerializedName;

import org.abh.photos.rest.ServiceFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by binhjy on 01/01/2017.
 */

public class PhotoModel {
    @SerializedName("_id")
    private String mId;
    @SerializedName("filename")
    private String mFilename;
    @SerializedName("destination")
    private String mDestination;
    @SerializedName("user_id")
    private String mUser_id;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("date")
    private Date mDate;

    private String mPhotoUrl;

    public PhotoModel(String id, String filename, String destination, String user_id, String status, Date date) {
        mId = id;
        mFilename = filename;
        mDestination = destination;
        mUser_id = user_id;
        mStatus = status;
        mDate = date;

    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getFilename() {
        return mFilename;
    }

    public void setFilename(String filename) {
        mFilename = filename;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getUser_id() {
        return mUser_id;
    }

    public void setUser_id(String user_id) {
        mUser_id = user_id;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Date getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String strDate = dateFormat.format(mDate);
        Date date = new Date();
        try {
            date = dateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getPhotoUrl(){
        mPhotoUrl = ServiceFactory.BASE_URL + ServiceFactory.PHOTO_API_URL + mId;
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl){mPhotoUrl = photoUrl;}

}
