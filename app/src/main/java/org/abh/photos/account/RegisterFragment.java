package org.abh.photos.account;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.abh.photos.R;
import org.abh.photos.rest.ServiceFactory;
import org.abh.photos.ui.FragmentAnimation;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements Callback<ResponseBody> {
    TextInputLayout mUsernameWrapper;
    TextInputLayout mEmailWrapper;
    TextInputLayout mPasswordWrapper;
    TextInputLayout mRePasswordWrapper;

    ProgressDialog mLoadingDialog;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        mUsernameWrapper = (TextInputLayout) view.findViewById(R.id.til_username);
        mEmailWrapper = (TextInputLayout) view.findViewById(R.id.til_email);
        mPasswordWrapper = (TextInputLayout) view.findViewById(R.id.til_password);
        mRePasswordWrapper = (TextInputLayout) view.findViewById(R.id.til_re_password);

        Button btnRegister = (Button) view.findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = getUserInput(mUsernameWrapper);
                String email = getUserInput(mEmailWrapper);
                String password = getUserInput(mPasswordWrapper);
                String rePassword = getUserInput(mRePasswordWrapper);
                doRegister(username, email, password, rePassword);
            }
        });

        return view;
    }

    private String getUserInput(TextInputLayout inputWrapper) {
        String userInput;
        EditText editText = inputWrapper.getEditText();
        if (editText != null) {
            userInput = editText.getText().toString();
        } else {
            userInput = "";
        }
        return userInput;
    }

    private void doRegister(String username, String email, String password, String rePassword) {
        // Perform client checking before send request to server
        boolean hasError = validateForm(username, email, password, rePassword);
        if (!hasError) {
            mLoadingDialog = ProgressDialog.show(getContext(), "Please wait...", "We are signing you up...");

            AuthService authService = ServiceFactory.createService(AuthService.class);
            Call<ResponseBody> registerCall = authService.register(new RegisterInfo(username, email, password));
            registerCall.enqueue(this);
        }
    }

    private boolean validateForm(String username, String email, String password, String rePassword) {
        boolean hasError = false;

        // Username must be provided and contain following characters: a-z, A-Z, 0-9, .-_
        String usernameRegex = "^[a-zA-Z0-9._-]+$";
        if (!username.matches(usernameRegex)) {
            mUsernameWrapper.setError("Username is invalid.");
            hasError = true;
        } else {
            mUsernameWrapper.setErrorEnabled(false);
        }

        // Email validation
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailWrapper.setError("Email is invalid.");
            hasError = true;
        } else {
            mEmailWrapper.setErrorEnabled(false);
        }

        // Password must be at least 6 characters and contain, least 1 number, least 1 character
        String passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d$@$!%*#?&]{6,}$";
        if (!password.matches(passwordRegex)) {
            mPasswordWrapper.setError("Password must be at least 6 characters, 1 digit and 1 letter.");
            hasError = true;
        } else {
            mPasswordWrapper.setErrorEnabled(false);
        }

        // Re-Password validation
        if (!rePassword.equals(password)) {
            mRePasswordWrapper.setError("Your password does not match.");
            hasError = true;
        } else {
            mRePasswordWrapper.setErrorEnabled(false);
        }

        return hasError;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
        mLoadingDialog.dismiss();
        if (response.isSuccessful()) {
            Toast.makeText(getContext(), "Register successfully", Toast.LENGTH_LONG).show();
            FragmentAnimation.navigateToFragment(getActivity(), new LoginFragment(), R.id.fragment_container_activity,
                    "LoginFragment", false);
        } else {
            String errorMessage;
            try {
                errorMessage = response.errorBody().string();
            } catch (IOException ex) {
                errorMessage = "Unexpected error.";
                Log.d("Register", "Unexpected error from server.");
            }
            // Check error received against pre-defined error messages
            String usernameConflictRegex = "^Username .* already exists.$";
            String emailConflictRegex = "^Email .* already exists.$";
            if (errorMessage.matches(usernameConflictRegex)) {
                mUsernameWrapper.setError(errorMessage);
            } else if (errorMessage.matches(emailConflictRegex)) {
                mEmailWrapper.setError(errorMessage);
            } else {
                Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
