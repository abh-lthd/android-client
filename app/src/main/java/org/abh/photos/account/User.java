package org.abh.photos.account;

import com.google.gson.annotations.SerializedName;

/**
 * Created by binhjy on 04/01/2017.
 */

public class User {
    @SerializedName("_id")
    String mId;

    @SerializedName("username")
    String mUsername;

    @SerializedName("email")
    String mEmail;

    public void setId(String id){mId = id;}

    public String getId(){return mId;}

    public void setUsername(String username){mUsername = username;}

    public String getUsername(){return mUsername;}

    public void setEmail(String email){mEmail = email;}

    public String getEmail(){return mEmail;}
}
