package org.abh.photos.account;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthService {
    @POST("/api/authentication/register")
    Call<ResponseBody> register(@Body RegisterInfo userInfo);

    @POST("/api/authentication/login")
    Call<AuthToken> login(@Body Credential credential);
}
