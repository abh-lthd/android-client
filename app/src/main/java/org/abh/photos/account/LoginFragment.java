package org.abh.photos.account;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.abh.photos.R;
import org.abh.photos.home.MainFragment;
import org.abh.photos.rest.ServiceFactory;
import org.abh.photos.ui.FragmentAnimation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements Callback<AuthToken> {


    private TextInputLayout mUsernameWrapper;
    private TextInputLayout mPasswordWrapper;

    private ProgressDialog mLoadingDialog;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mUsernameWrapper = (TextInputLayout) view.findViewById(R.id.til_username);
        mPasswordWrapper = (TextInputLayout) view.findViewById(R.id.til_password);

        Button btnLogin = (Button) view.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = getUserInput(mUsernameWrapper);
                String password = getUserInput(mPasswordWrapper);
                doLogin(username, password);
            }
        });

        LoginButton btnFbLogin = (LoginButton) view.findViewById(R.id.btn_fb_login);
        btnFbLogin.setFragment(this);
        CallbackManager callbackManager = CallbackManager.Factory.create();
        btnFbLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(getContext(), loginResult.getAccessToken().getToken(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getContext(), "Cancel", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        TextView tvSignUpNow = (TextView) view.findViewById(R.id.tv_sign_up_now);
        tvSignUpNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentAnimation.navigateToFragment(getActivity(), new RegisterFragment(), R.id.fragment_container_activity,
                        "RegisterFragment", true);
            }
        });

        return view;
    }

    private String getUserInput(TextInputLayout inputWrapper) {
        String userInput;
        EditText editText = inputWrapper.getEditText();
        if (editText != null) {
            userInput = editText.getText().toString();
        } else {
            userInput = "";
        }
        return userInput;
    }

    private void doLogin(String username, String password) {
        if (validateForm(username, password)) {
            mLoadingDialog = ProgressDialog.show(getContext(), "Please wait...", "We are signing you in...");

            AuthService authService = ServiceFactory.createService(AuthService.class);
            Call<AuthToken> loginCall = authService.login(new Credential(username, password));
            loginCall.enqueue(this);
        }
    }

    private boolean validateForm(String username, String password) {
        boolean validForm = true;

        // Username must be provided and contain following characters: a-z, A-Z, 0-9, .-_
        String usernameRegex = "^[a-zA-Z0-9._-]+$";
        if (!username.matches(usernameRegex)) {
            mUsernameWrapper.setError("Username is invalid.");
            validForm = false;
        } else {
            mUsernameWrapper.setErrorEnabled(false);
        }

        // Password must be at least 6 characters and contain, least 1 number, least 1 character
        String passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d$@$!%*#?&]{6,}$";
        if (!password.matches(passwordRegex)) {
            mPasswordWrapper.setError("Password must be at least 6 characters, 1 digit and 1 letter.");
            validForm = false;
        } else {
            mPasswordWrapper.setErrorEnabled(false);
        }

        return validForm;
    }

    @Override
    public void onResponse(Call<AuthToken> call, Response<AuthToken> response) {
        mLoadingDialog.dismiss();
        if (response.isSuccessful()) {
            String token = response.body().getToken();
            User user = response.body().getUser();
            CredentialManager.getInstance().setAuthToken(token);
            CredentialManager.getInstance().setUsername(user.getUsername());
            CredentialManager.getInstance().setEmail(user.getEmail());
            CredentialManager.getInstance().setId(user.getId());

            FragmentAnimation.navigateToFragment(getActivity(), new MainFragment(), R.id.fragment_container_activity,
                    "MainFragment", false);

        } else {
            Toast.makeText(getContext(), "Username or password is not correct.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<AuthToken> call, Throwable t) {
        mLoadingDialog.dismiss();
        Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
