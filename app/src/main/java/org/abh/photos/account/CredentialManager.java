package org.abh.photos.account;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class CredentialManager {
    private static CredentialManager sInstance;

    private String mAuthToken;
    private String mUsername;
    private String mEmail;
    private String mId;

    private CredentialManager() {

    }

    public static CredentialManager getInstance() {
        if (sInstance == null) {
            sInstance = new CredentialManager();
        }
        return sInstance;
    }

    public String getAuthToken() {
        return mAuthToken;
    }

    public void setAuthToken(String authToken) {
        mAuthToken = authToken;
    }

    public void setUsername(String username){mUsername = username;}

    public String getUsername(){return mUsername;}

    public void setEmail(String email){mEmail = email;}

    public String getEmail(){return mEmail;}

    public void setId(String id){mId = id;}

    public String getId(){return mId;}


}
