package org.abh.photos.account;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class AuthToken {
    @SerializedName("token")
    private String mToken;

    @SerializedName("user")
    private User mUser;

    public AuthToken() {}

    public AuthToken(String token) {
        mToken = token;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public void setUser(User user){mUser = user;}

    public User getUser(){return mUser;}
}
