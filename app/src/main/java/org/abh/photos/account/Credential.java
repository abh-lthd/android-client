package org.abh.photos.account;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class Credential {
    @SerializedName("username")
    private String mUsername;

    @SerializedName("password")
    private String mPassword;

    public Credential(String username, String password) {
        mUsername = username;
        mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }
}
