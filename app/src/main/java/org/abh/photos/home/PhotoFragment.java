package org.abh.photos.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import com.getbase.floatingactionbutton.FloatingActionButton;

import org.abh.photos.R;
import org.abh.photos.account.CredentialManager;
import org.abh.photos.photo.photoCallback.GetAllPhotosServiceCallback;
import org.abh.photos.photo.photoModel.Photo;
import org.abh.photos.photo.photoFragment.PhotoChoosingDialogFragment;
import org.abh.photos.photo.photoInterface.PhotoService;
import org.abh.photos.photo.photoFragment.UploadFragment;
import org.abh.photos.photo.photoCallback.UploadServiceCallback;
import org.abh.photos.rest.ServiceFactory;
import org.abh.photos.ui.FragmentAnimation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoFragment extends Fragment {
    public static final int REQUEST_CHOOSE_PHOTO = 1;
    private static final int REQUEST_PHOTO_CAPTURE = 2;
    private static final int REQUEST_PHOTO_SELECT = 3;
    private static String mCurrentPhotoPath;

    private GridView mGvPhoto;
    private ListView mLvPhoto;
    private FloatingActionButton mFabCamera;
    private FloatingActionButton mFabLibrary;

    private PhotoChoosingDialogFragment dialogFragment = new PhotoChoosingDialogFragment();
    private static int mSortChoice = 0;
    private static boolean mIsViewOnGrid = false;
    private static MenuItem mMenuItemViewPhoto = null;

    public PhotoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        mGvPhoto = (GridView)view.findViewById(R.id.photo_gvPhoto);
        mLvPhoto = (ListView)view.findViewById(R.id.photo_lvPhoto);
        mFabCamera = (FloatingActionButton)view.findViewById(R.id.photo_fabCamera);
        mFabLibrary = (FloatingActionButton)view.findViewById(R.id.photo_fabLibrary);
//        mFabUpload = (FloatingActionButton)view.findViewById(R.id.home_fabUpload);
//        mFabUpload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialogFragment.setTargetFragment(PhotoFragment.this, REQUEST_CHOOSE_PHOTO);
//                dialogFragment.show(getActivity().getSupportFragmentManager(), "PhotoChoosingDialogFragment");
//            }
//        });
        mFabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoCamera();
            }
        });

        mFabLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoLibrary();
            }
        });


//        if(mMenu != null) {
//            MenuItem item = mMenu.getItem(R.id.home_menu_viewPhoto);
//            ViewPhoto(item);
//        }
        // Show photo of current user

        //setHasOptionsMenu(true);

        return view;
    }



    public void onPhotoReadyForUpload(Bitmap bitmap) {
        //dialogFragment.dismiss();
        PhotoService photoService = ServiceFactory.createService(PhotoService.class);

        String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        RequestBody photo = RequestBody.create(MediaType.parse("image/*"), byteArray);
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), "gg");

        Call<Photo> uploadCall = photoService.upload(authorization, photo, status);
        uploadCall.enqueue(new UploadServiceCallback(getActivity()));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_menu, menu);
        mMenuItemViewPhoto = menu.findItem(R.id.home_menu_viewPhoto);
        GetAllPhotosServiceCallback.LoadPhoTo(getActivity(), mGvPhoto, mLvPhoto, mMenuItemViewPhoto);
        //menu.clear();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.home_menu_viewPhoto:{
                GetAllPhotosServiceCallback.ViewPhoto();
                break;
            }
            case R.id.home_menu_sort:{
                SortPhoto();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // MenuItem
    //region
//    private void ViewPhoto(MenuItem item) {
//        if(!mViewOnGrid) {
//            mViewOnGrid = true;
//            mLvPhoto.setVisibility(View.VISIBLE);
//            mGvPhoto.setVisibility(View.GONE);
//            GetAllPhotosServiceCallback.ViewPhoto(mViewOnGrid);
//            item.setIcon(R.drawable.ic_gridview);
//            item.setTitle("ic_listview");
//
//        }
//        else {
//            mViewOnGrid = false;
//            mLvPhoto.setVisibility(View.GONE);
//            mGvPhoto.setVisibility(View.VISIBLE);
//            GetAllPhotosServiceCallback.ViewPhoto(mViewOnGrid);
//
//            item.setIcon(R.drawable.ic_listview);
//            item.setTitle("ic_gridview");
//
//        }
//        return;
//    }

    private void SortPhoto(){
        CharSequence[] arrLstSort = {"Date(newer-older)", "Date(older-newer)"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Sort by")
                .setSingleChoiceItems(arrLstSort, mSortChoice, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mSortChoice = i;
                        if(i == 0) {
                            GetAllPhotosServiceCallback.SortListPhoTo(GetAllPhotosServiceCallback.SORTMODE.AS);
                        }
                        else
                            GetAllPhotosServiceCallback.SortListPhoTo(GetAllPhotosServiceCallback.SORTMODE.DES);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.create().show();
    }
    //endregion

    public void PhotoCamera(){
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePhotoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(), "org.abh.photos", photoFile);
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePhotoIntent, REQUEST_PHOTO_CAPTURE);
            }
        }
    }

    private void PhotoLibrary(){
        Intent selectPhotoIntent = new Intent();
        selectPhotoIntent.setType("image/*");
        selectPhotoIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(selectPhotoIntent, "Select Photo"), REQUEST_PHOTO_SELECT);
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFilename = "JPEG_" + timeStamp;
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFilename, ".png", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bitmap bitmapPhoto = null;

            if (requestCode == REQUEST_PHOTO_CAPTURE) {
                bitmapPhoto = BitmapFactory.decodeFile(mCurrentPhotoPath);
            } else if (requestCode == REQUEST_PHOTO_SELECT) {
                Uri selectedPhotoUri = data.getData();
                try {
                    bitmapPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedPhotoUri);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            Fragment fmUpload = new UploadFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("bPhoto", byteArray);
            fmUpload.setArguments(bundle);
            FragmentAnimation.navigateToFragment(getActivity(), fmUpload, R.id.fragment_container,
                    "UploadFragment", true);
        }
    }

}
