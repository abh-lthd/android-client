package org.abh.photos.home;

import com.google.gson.annotations.SerializedName;

import org.abh.photos.rest.ServiceFactory;
import org.abh.photos.search.UserModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by binhjy on 08/01/2017.
 */

public class PhotoFollowingModel {
    @SerializedName("_id")
    String mId;
    @SerializedName("filename")
    String mFilename;
    @SerializedName("destination")
    String mDestination;
    @SerializedName("user_id")
    String mUserId;
    @SerializedName("status")
    String mStatus;
    @SerializedName("date")
    Date mDate;
    @SerializedName("uploader")
    UserModel mUploader;

    private String mPhotoUrl;

    public PhotoFollowingModel(String id, String filename, String destination, String user_id, String status, Date date,
                               UserModel userModel) {
        mId = id;
        mFilename = filename;
        mDestination = destination;
        mUserId = user_id;
        mStatus = status;
        mDate = date;
        mUploader = userModel;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getFilename() {
        return mFilename;
    }

    public void setFilename(String filename) {
        mFilename = filename;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String user_id) {
        mUserId = user_id;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Date getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String strDate = dateFormat.format(mDate);
        Date date = new Date();
        try {
            date = dateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getPhotoUrl(){
        mPhotoUrl = ServiceFactory.BASE_URL + ServiceFactory.PHOTO_API_URL + mId;
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl){mPhotoUrl = photoUrl;}

    public void setUploader(UserModel uploader){mUploader = uploader;}

    public UserModel getUploader(){return mUploader;}

}
