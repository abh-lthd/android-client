package org.abh.photos.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.abh.photos.R;
import org.abh.photos.account.CredentialManager;
import org.abh.photos.photo.photoAdapter.ListPhotoAdapter;
import org.abh.photos.photo.photoModel.PhotoModel;
import org.abh.photos.profile.FollowInterface;
import org.abh.photos.rest.ServiceFactory;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by binhjy on 08/01/2017.
 */

public class HomeFragment extends Fragment implements Callback<List<PhotoFollowingModel>> {
    private static ArrayList<PhotoFollowingModel> mLstPhotoFollow;
    private ListView mLvPhoto;
    private ListPhotoAdapter mListPhotoAdapter;
    private ArrayList<PhotoModel> mLstPhoto;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mLvPhoto = (ListView)view.findViewById(R.id.home_lvPhoto);

        ListPhotoAdapter mListPhotoAdapter = null;
        mLstPhotoFollow = new ArrayList<>();
        mLstPhoto = new ArrayList<>();

        GetPhotoFollowing();

        return view;
    }


    private void GetPhotoFollowing() {
        FollowInterface followInterface = ServiceFactory.createService(FollowInterface.class);
        String authorization = "Bearer " + CredentialManager.getInstance().getAuthToken();
        Call<List<PhotoFollowingModel>> getPhotoFollowCall = followInterface.getAllPhotoFollowing(authorization);
        getPhotoFollowCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<PhotoFollowingModel>> call, Response<List<PhotoFollowingModel>> response) {
        if(response.isSuccessful()){
            if(response.body().size() > 0) {
                mLstPhotoFollow.clear();
                for(PhotoFollowingModel followingModel : response.body()){
                    mLstPhotoFollow.add(followingModel);
                }
                ShowPhotoFollowing();
            }
        }
        else {
            Toast.makeText(getContext(), "GetPhotoFollowing failed", Toast.LENGTH_LONG).show();
        }
    }

    private void ShowPhotoFollowing() {
        if(mLstPhotoFollow.size() > 0){
            for(PhotoFollowingModel photoFollowingModel : mLstPhotoFollow){
                mLstPhoto.add(new PhotoModel(photoFollowingModel.getId(), photoFollowingModel.getFilename(), photoFollowingModel.getDestination(),
                        photoFollowingModel.getUserId(), photoFollowingModel.getStatus(), photoFollowingModel.getDate()));
            }
            mListPhotoAdapter = new ListPhotoAdapter(getActivity(), mLstPhoto, mLvPhoto);
            mLvPhoto.setAdapter(mListPhotoAdapter);
        }
    }

    @Override
    public void onFailure(Call<List<PhotoFollowingModel>> call, Throwable t) {
        Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }
}
