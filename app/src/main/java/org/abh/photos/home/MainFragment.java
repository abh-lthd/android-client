package org.abh.photos.home;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import org.abh.photos.R;
import org.abh.photos.search.SearchFragment;
import org.abh.photos.ui.FragmentAnimation;

/**
 * Created by binhjy on 04/01/2017.
 */

public class MainFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);

        BottomBar bottomBar = (BottomBar)view.findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId){
                    case R.id.tab_home:{
                        FragmentAnimation.navigateToFragment(getActivity(), new HomeFragment(), R.id.fragment_container,
                                "HomeFragment", false);


                        break;
                    }
                    case R.id.tab_camera:{

                        break;
                    }
                    case R.id.tab_search:{
                        FragmentAnimation.navigateToFragment(getActivity(), new SearchFragment(), R.id.fragment_container,
                                "SearchFragment",true);
                        break;
                    }
                    case R.id.tab_photo:{
                        FragmentAnimation.navigateToFragment(getActivity(), new PhotoFragment(), R.id.fragment_container,
                                "PhotoFragment",true);
                        break;
                    }
                    case R.id.tab_profile:{

//                        FragmentAnimation.navigateToFragment(getActivity(), new ProfileFragment(), R.id.fragment_container,
//                                "ProfileFragment", true);
                        break;
                    }
                    default:
                        break;
                }
            }
        });



        return view;
    }

}
