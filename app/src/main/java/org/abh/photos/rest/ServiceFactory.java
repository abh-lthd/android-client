package org.abh.photos.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chuzzz on 1/1/2017.
 */

public class ServiceFactory {
    public static final String BASE_URL = "http://192.168.1.48:3000";
    public static String PHOTO_API_URL = "/api/photos/";

    private static Retrofit builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static <T> T createService(Class<T> serviceClass) {
        return builder.create(serviceClass);
    }
}
